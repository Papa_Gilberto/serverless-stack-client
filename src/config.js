export default {
  MAX_ATTACHMENT_SIZE: 5000000,
  s3: {
    REGION: "us-east-2",
    BUCKET: "notes-app-api-prod-serverlessdeploymentbucket-1fsltv3ril71e"
  },
  apiGateway: {
    REGION: "us-east-2",
    URL: "https://p336brtyth.execute-api.us-east-2.amazonaws.com/prod"
  },
  cognito: {
    REGION: "us-east-2",
    USER_POOL_ID: "us-east-2_fhggM4r23",
    APP_CLIENT_ID: "rgpce1c0p6spspeuqeh1e5oht",
    IDENTITY_POOL_ID: "us-east-2:d077b36b-c299-4cfe-b7cc-dc8fab55fa3f"
  }
};